#!/usr/bin/guile3.0

!#


(define-module (hyperbuilder builder machine)
  #:use-module (oop goops)
  #:use-module (ice-9 exceptions)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder help cmd)
  #:export (<machine>
            get-run-cmd
            machine-cpu-count
            machine-ram-size-bytes
            machine-chipset
            machine-archecture
            machine-cdrom))


(define-class <machine> ()
  (cpu-count #:accessor machine-cpu-count #:init-keyword #:cpu-count #:init-value 1)
  (ram-size-bytes #:accessor machine-ram-size-bytes #:init-keyword #:ram-size-bytes #:init-value (* 1000 1000 1000))
  (chipset #:accessor machine-chipset #:init-keyword #:chipset #:init-value 'PIIX3)
  (archecture #:accessor machine-archecture #:init-keyword #:archecture #:init-value 'x86_64)
  (cdrom #:accessor machine-cdrom #:init-keyword #:cdrom #:init-value ""))

(define-generic make-machine)
(export make-machine)
(define-method (make-machine cpu-count ram-size-bytes chipset archecture cdrom)
  (make <machine>
    #:cpu-count cpu-count
    #:ram-size-bytes ram-size-bytes
    #:chipset chipset
    #:archecture archecture
    #:cdrom cdrom))


(define-generic get-run-cmd)
