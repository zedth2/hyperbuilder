#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder builder qemu archs)
  #:use-module (oop goops)
  #:use-module (ice-9 exceptions)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder help cmd)
  #:use-module (hyperbuilder builder machine)
  #:use-module (hyperbuilder builder storage)
  #:use-module (hyperbuilder builder qemu storage)
  #:export (get-arch-command))

(define archs '((x86_64 . "qemu-system-x86_64")))

(define (get-arch-command arch)
    (cdr (assoc arch archs)))
