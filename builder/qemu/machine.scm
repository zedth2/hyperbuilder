#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder builder qemu machine)
  #:use-module (oop goops)
  #:use-module (ice-9 exceptions)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder help cmd)
  #:use-module (hyperbuilder builder machine)
  #:use-module (hyperbuilder builder storage)
  #:use-module (hyperbuilder builder qemu storage)
  #:use-module (hyperbuilder builder qemu archs)
  #:export (get-run-cmd <qemu-machine> make-machine))

(define-class <qemu-machine> (<machine>)
  (ram-slots #:accessor qemu-machine-ram-slots #:init-keyword #:ram-slots #:init-value 0)
  (ram-max-mem #:accessor qemu-machine-ram-max-mem #:init-keyword #:max-mem #:init-value 0)
  (max-cpus #:accessor qemu-machine-max-cpus #:init-keyword #:max-cpu #:init-value 0)
  (cpu-sockets #:accessor qemu-machine-cpu-sockets #:init-keyword #:cpu-sockets #:init-value 0)
  (cpu-dies #:accessor qemu-machine-cpu-dies #:init-keyword #:cpu-dies #:init-value 0)
  (cpu-cores #:accessor qemu-machine-cpu-cores #:init-keyword #:cpu-cores #:init-value 0)
  (cpu-threads #:accessor qemu-machine-cpu-threads #:init-keyword #:cpu-threads #:init-value 0))

(define-method (make-machine cpu-count ram-size-bytes chipset archecture cdrom)
  (make <qemu-machine>
    #:cpu-count cpu-count
    #:ram-size-bytes ram-size-bytes
    #:chipset chipset
    #:archecture archecture
    #:cdrom cdrom))

; qemu-system-x86_64 poops.img -m 4G  -cdrom ~/os_iso/archlinux-2021.08.01-x86_64.iso -smp 2

(define (get-mem-switch machine)
  "-m [size=]megs[,slots=n,maxmem=size]"
  (let* ((mb (bytes->megabytes (machine-ram-size-bytes machine)))
         (size-str (number->string mb))
         (slots (qemu-machine-ram-slots machine))
         (slotstr (if (zero? slots) "" (string-concatenate (list ",slots=" (number->string slots)))))
         (maxmem (qemu-machine-ram-max-mem machine))
         (maxmemstr (if (zero? maxmem) "" (string-concatenate (list ",maxmem=" (bytes->megabytes maxmem) "M")))))
    (string-concatenate (list "-m size=" size-str "M" slotstr maxmemstr))))

(define (get-cpu-switch machine)
  "-smp [[cpus=]n][,maxcpus=maxcpus][,sockets=sockets][,dies=dies][,cores=cores][,threads=threads]"
  (let* ((cnt (number->string (machine-cpu-count machine)))
         (maxcpus (qemu-machine-max-cpus machine))
         (maxcpustr (if (zero? maxcpus) "" (string-concatenate (list ",maxcpus=" (number->string maxcpus)))))
         (sockets (qemu-machine-cpu-sockets machine))
         (socketstr (if (zero? sockets) "" (string-concatenate (list ",sockets=" (number->string sockets)))))
         (dies (qemu-machine-cpu-dies machine))
         (diestr (if (zero? dies) "" (string-concatenate (list ",dies=" (number->string dies)))))
         (cores (qemu-machine-cpu-cores machine))
         (corestr (if (zero? cores) "" (string-concatenate (list ",cores=" (number->string cores)))))
         (threads (qemu-machine-cpu-threads machine))
         (threadstr (if (zero? threads) "" (string-concatenate (list ",threads=" (number->string threads))))))
    (string-concatenate (list "-smp cpus=" cnt maxcpustr socketstr diestr corestr threadstr))))

(define (get-cdrom-switch machine)
  (let ((path (machine-cdrom machine)))
    (if (string=? "" path) ""
        (string-concatenate (list "-cdrom " path)))))

(define-method (get-run-cmd machine storage)
  (let* ((mem (get-mem-switch machine))
         (cpu (get-cpu-switch machine))
         (cdrom (get-cdrom-switch machine))
         (cmd (list (get-arch-command (machine-archecture machine)) (storage-filename storage) mem cpu cdrom)))
    cmd))
