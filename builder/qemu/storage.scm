#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder builder qemu storage)
  #:use-module (oop goops)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 optargs)
  #:use-module (rnrs io ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 string-fun)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder help cmd)
  #:use-module (hyperbuilder builder storage)
  #:export (make-storage create-img-cmd-str create-img-cmd create-img))

(define QEMU_IMG_CMD "qemu-img")

(define-class <qemu-storage> (<storage>))

(define* (make-storage
          filename
          #:key
          (type "disk")
          (size-bytes 4000256)
          (sector-size 512)
          (vm-format "qcow2")
          (variant "Standard"))
  (make
      <qemu-storage>
    #:filename filename
    #:type type
    #:size-bytes size-bytes
    #:sector-size sector-size
    #:vm-format vm-format
    #:variant variant))

(define-generic create-img-cmd)
(define-method (create-img-cmd (qstorage <qemu-storage>))
  (list QEMU_IMG_CMD "create" "-f" (storage-vm-format qstorage) (storage-filename qstorage) (number->string (storage-size-bytes qstorage))))

(define-generic create-exe-command)
(define-method (create-exe-command (qstorage <qemu-storage>))
    (apply build-command (create-img-cmd qstorage)))

(define-generic create-img)
(define-method (create-img (qstorage <qemu-storage>))
  (let ((cmd (call-command-with-output-error-to-string (create-exe-command qstorage))))
    (print cmd) (print (command-exit-val cmd))
    (cond
     ((not (zero? (command-exit-val cmd)))
      (let ((ex (make-storage-exception qstorage
                                        'qemu
                                        (command-cmd cmd)
                                        (string-concatenate (list "failed to make storage\n"
                                                            (storage-error-parse (command-stderr cmd)) (command-stderr cmd))))))
                                                            ;(command-stderr cmd)
        (raise-exception ex)))
     (else (print "Success")))))

;; Example:
;; qemu-img: poops.img: Image creation needs a size parameter
(define (storage-error-parse err-str)
  (let ((split (string-split err-str #\:)))
    (if (< 2 (length split)) (string-trim-both (caddr split)) err-str)))

(define-generic create-img-cmd-str)
(define-method (create-img-cmd-str (qstorage <qemu-storage>))
  (list->string-cmd (create-img-cmd qstorage)))

;(define-generic modify-img-cmd)
;(define-method (modify-img-cmd (storage <storage>))
;  (list QEMU_IMG_CMD ""))


(define s "0%...
Progress state: VBOX_E_FILE_ERROR
VBoxManage: error: Failed to create medium
VBoxManage: error: Could not create the medium storage unit '/home/zac/shitz.vdi'.
VBoxManage: error: VDI: cannot create image '/home/zac/shitz.vdi' (VERR_ALREADY_EXISTS)
VBoxManage: error: Details: code VBOX_E_FILE_ERROR (0x80bb0004), component MediumWrap, interface IMedium
VBoxManage: error: Context: \"RTEXITCODE handleCreateMedium(HandlerArg*)\" at line 510 of file VBoxManageDisk.cpp
zsh: exit 1     vboxmanage createmedium disk --filename shitz --sizebyte 4000256 --format VDI")

(map
 (lambda (line)
   (display
    (string-concatenate
     (list ""
           ;(string-replace-substring (string-downcase (car (string-split line #\:))) " " "-")
           "((eqv? *" (string-replace-substring (string-downcase (car (string-split line #\:))) " " "-") "* name) (set! " (string-replace-substring (string-downcase (car (string-split line #\:))) " " "-") " info))\n"))))
 (string-split s #\newline))
