#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder builder storage)
  #:use-module (oop goops)
  #:use-module (ice-9 exceptions)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder help cmd)
  #:export (make-storage-exception
            <storage>
            <storage-exception>
            storage-type
            storage-filename
            storage-size-bytes
            storage-sector-size
            storage-vm-format
            storage-variant
            ;storage->human-str
            set-size-bytes
            set-sector-size
            align-sector
            align-sector!))

(define (make-storage-exception storage-obj engine cmd msg)
  (make-exception (make-hyper-storage-exception engine cmd storage-obj) (make-exception-with-message msg)))

(define-class <storage> ()
  (type #:accessor storage-type #:init-keyword #:type #:init-value "disk")
  (filename #:accessor storage-filename #:init-keyword #:filename)
  (size-bytes #:accessor storage-size-bytes #:setter set-size-bytes #:init-keyword #:size-bytes #:init-value 4000256) ; Must be at least 4MB
  (sector-size #:accessor storage-sector-size #:setter set-sector-size #:init-keyword #:sector-size #:init-value 512)
  (vm-format #:accessor storage-vm-format #:init-keyword #:vm-format #:init-value "VDI")
  (variant #:accessor storage-variant #:init-keyword #:variant #:init-value "Standard")
  (attach-vm #:accessor storage-attach-vm #:init-keyword #:attach-vm #:init-value '()))

(define (make-storage type filename size-bytes sector-size vm-foramt variant attach-vm)
  (make <storage>
    #:type type
    #:filename filename
    #:size-bytes size-bytes
    #:sector-size sector-size
    #:vm-format vm-format
    #:variant variant
    #:attach-vm attach-vm))

(define-generic alist->storage)
(export alist->storage)
(define-method (alist->storage lst)
  (let* ((type (cdr (assoc 'type lst)))
         (filename (cdr (assoc 'filename lst)))
         (size-bytes (cdr (assoc 'size-bytes lst)))
         (sector-size (cdr (assoc 'sector-size lst)))
         (vm-format (cdr (assoc 'vm-format lst)))
         (variant (cdr (assoc 'variant lst)))
         (attach-vm (cdr (assoc 'attach-vm lst))))
    (make-storage type filename size-bytes sector-size vm-format variant attach-vm)))

;(define-generic storage->alist)
;(export storage->alist)
;(define-method (storage->alist stor)
;  '((type . (storage-type stor))
;    (filename . (storage-filename stor))
;    (size-bytes . (storage-size-bytes stor))
;    (sector-size . (storage-sector-size stor))
;    (vm-format . (storage-vm-format stor))
;    (variant . (storage-variant stor))
;    (attach-vm . (storage-attach-vm stor))))

(define-generic storage->human-str)
(export storage->human-str)
(define-method (storage->human-str (storage <storage>))
  (list
   "Filename: " (storage-filename storage) "\n"
   "Type: " (storage-type storage) "\n"
   "Size Bytes: " (storage-size-bytes storage) "\n"
   "Sector Size: " (storage-sector-size storage) "\n"
   "Format: " (storage-vm-format storage) "\n"
   "Variant: " (storage-variant storage) "\n"
   "Attach VM: " (storage-attach-vm storage) "\n"))

(define-generic storage->pretty-alist)
(export storage->pretty-alist)

(define-generic align-sector)
(define-method (align-sector (storage <storage>))
  (let ((size (size-bytes storage))
        (sector (sector-size storage)))
    (+ size (- sector (modulo size sector)))))

(define-generic align-sector!)
(define-method (align-sector! (storage <storage>))
  (set-size-bytes
   storage
   (let ((size (storage-size-bytes storage))
   (sector (storage-sector-size storage)))
     (+ size (- sector (modulo size sector))))))
