#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder builder vbox storage)
  #:use-module (hyperbuilder builder storage)
  #:use-module (hyperbuilder help cmd)
  #:use-module (hyperbuilder help)
  #:use-module (oop goops)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 regex)
  #:use-module (rnrs io ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 receive)
  #:use-module (hyperbuilder help)
  #:export     (get-vms
                make-storage
                make-vbox-storage
                <vbox-storage>
                create-img
                make-storage-from-str
                create-img-cmd
                create-img-cmd-str
                )
  #:re-export (
               storage->human-str
               ))


;UUID:           512dd25e-956c-4384-95f9-875054c10eed
;Parent UUID:    2fea7c83-0e25-4b32-aa39-9c5738dfb874
;State:          created
;Type:           normal (differencing)
;Auto-Reset:     off
;Location:       /home/zac/VirtualBox VMs/clienttest/Snapshots/{512dd25e-956c-4384-95f9-875054c10eed}.vdi
;Storage format: VDI
;Format variant: differencing default
;Capacity:       32768 MBytes
;Size on disk:   2329 MBytes
;Encryption:     disabled
;Property:       AllocationBlockSize=1048576
;In use by VMs:  clienttest (UUID: da7cff78-0159-4c74-8ea1-668e4a4f7e55)


(define VBOX_CMD "vboxmanage")

(define-class <vbox-storage> (<storage>)
  (uuid #:accessor vbox-storage-uuid #:init-keyword #:uuid #:init-value "")
  (parent-uuid #:accessor vbox-storage-parent-uuid #:init-keyword #:parent-uuid #:init-value "")
  (encryption #:accessor vbox-storage-encryption #:init-keyword #:encryption #:init-value #f))

(define (slot<? left right)
  (string<?
   (symbol->string (slot-definition-name left))
   (symbol->string (slot-definition-name right))))

;(define-generic storage->human-str)
(define-method (storage->human-str (storage <vbox-storage>))
  (append (next-method) (list
                         "UUID: " (vbox-storage-uuid storage) "\n"
                         "Parent UUID: " (vbox-storage-parent-uuid storage) "\n"
                         "Encryption: " (vbox-storage-encryption storage) "\n")))


(define* (make-vbox-storage
                       #:key
                       (filename "")
                       (size-bytes 4000000)
                       (type "disk")
                       (vm-format "VDI")
                       (variant "Standard")
                       (sector-size 512)
                       (size-fix #t)
                       (uuid "")
                       (parent-uuid "")
                       (encryption #f))
  (let
      ((new-storage (make <vbox-storage>
                      #:filename filename
                      #:size-bytes size-bytes
                      #:sector-size sector-size
                      #:type type
                      #:vm-format vm-format
                      #:variant variant
                      #:uuid uuid
                      #:parent-uuid parent-uuid
                      #:encryption encryption)))
    (if size-fix (align-sector! new-storage))
    new-storage))
(define* (make-storage filename size-bytes
                       #:key
                       (type "disk")
                       (vm-format "VDI")
                       (variant "Standard")
                       (sector-size 512)
                       (size-fix #t)
                       (uuid "")
                       (parent-uuid "")
                       (encryption #f))
  (let
      ((new-storage (make <vbox-storage>
                      #:filename filename
                      #:size-bytes size-bytes
                      #:sector-size sector-size
                      #:type type
                      #:vm-format vm-format
                      #:variant variant
                      #:uuid uuid
                      #:parent-uuid parent-uuid
                      #:encryption encryption)))
    (if size-fix (align-sector! new-storage))
    new-storage))

(define *uuid* "UUID")
(define *parent-uuid* "Parent UUID")
(define *state* "State")
(define *type* "Type")
(define *auto-reset* "Auto-Reset")
(define *location* "Location")
(define *storage-format* "Storage format")
(define *format-variant* "Format variant")
(define *capacity* "Capacity")
(define *size-on-disk* "Size on disk")
(define *encryption* "Encryption")
(define *property* "Property")
(define *in-use-by-vms* "In use by VMs")

(define (make-storage-from-str str)
  (let ((uuid "")
        (parent-uuid "")
        (state "")
        (type "")
        (auto-reset "")
        (location "")
        (storage-format "")
        (format-variant "")
        (capacity "")
        (size-on-disk "")
        (encryption "")
        (property "")
        (in-use-by-vms ""))
    (newline-pred str
                  (lambda (line)
                    (let* ((split (string-split line #\:))
                           (name (string-trim-both(car split)))
                           (info (string-trim-both (cadr split))))
                      (cond
                       ((eqv? *uuid* name) (set! uuid info))
                       ((eqv? *parent-uuid* name) (set! parent-uuid info))
                       ((eqv? *state* name) (set! state info))
                       ((eqv? *type* name) (set! type info))
                       ((eqv? *auto-reset* name) (set! auto-reset info))
                       ((eqv? *location* name) (set! location info))
                       ((eqv? *storage-format* name) (set! storage-format info))
                       ((eqv? *format-variant* name) (set! format-variant info))
                       ((eqv? *capacity* name) (set! capacity info))
                       ((eqv? *size-on-disk* name) (set! size-on-disk info))
                       ((eqv? *encryption* name) (set! encryption info))
                       ((eqv? *property* name) (set! property info))
                       ((eqv? *in-use-by-vms* name) (set! in-use-by-vms info))))))
    (make-storage
     location
     (to-bytes capacity)
     #:type type
     #:vm-format storage-format
     #:size-fix #f
     #:uuid uuid
     #:parent-uuid parent-uuid
     #:encryption encryption)))


(define (to-bytes str) str)


(define-generic delete-img-cmd)
(define-method (delete-img-cmd (storage <storage>))
  (list VBOX_CMD "closemedium" (storage-filename storage) "--delete"))

;(define-generic build-storage)
(define-generic create-img-cmd)
(define-method (create-img-cmd (storage <vbox-storage>))
  (list VBOX_CMD "createmedium" (storage-type storage)
  "--filename" (storage-filename storage)
  "--sizebyte" (number->string (storage-size-bytes storage))
  "--format" (storage-vm-format storage)
  "--variant" (storage-variant storage)))

(define-generic create-exe-cmd)
(define-method (create-exe-cmd (storage <storage>))
  (apply build-command (create-img-cmd storage)))

(define-generic create-img-cmd-str)
(define-method (create-img-cmd-str (storage <storage>))
  (list->string-cmd (create-img-cmd storage)))

(define (check-for-error text)
  (let ((lines (string-split text #\lf))
        (res ""))
    (fold (lambda (line lst)
            (if
             (string-starts-with "Progress state:" line #:ci #t)
             (cons (string-trim-both (cadr (string-split line #\:))) lst) lst)) '() lines)))

;(define-generic create-img)
;(define-method (create-img (storage <vbox-storage>))
;  (let ((cmd (call-command-with-output-error-to-string (create-exe-cmd storage))))
;    (print (cmd)) (print (check-for-error (command-stderr storage)))))

(define-generic create-img)
(define-method (create-img (storage <vbox-storage>))
  (let* ((cmd (call-command-with-output-error-to-string (apply build-command (create-img-cmd storage))))
         (is-err (match-verr (command-stderr cmd))))
    (if (not (eq? "" is-err))
        (let ((ex
               (make-storage-exception storage
                                       'vbox
                                       (command-cmd cmd)
                                       (string-concatenate (list "failed to make storage\n"
                                                                 (match-verr
                                                                  (command-stderr cmd))
                                                                 (command-stderr cmd))))))
          (raise-exception ex)) (print "Success"))))

(define verr-regex (make-regexp "(VERR[_A-Z]+)"))
(define (match-verr str)
  (let* ((fnd (regexp-exec verr-regex str)))
    (if fnd (match:substring fnd) "")))

(define list-mediums-cmd-str (list->string-cmd (list VBOX_CMD "list" "hdds")))

;(define* (find-all-mediums)
;  (receive (out err)
;      (call-command-with-output-error-to-string list-mediums-cmd-str)))

;(define* (string->storage text)

;(define-method (create-storage (storage <storage>))
;  (let* ((port (with-error-to-port
;     (apply open-pipe* OPEN_READ (create-medium-cmd storage))
;     (lambda () (read-delimited ""))
;    (err #f))))))


;(define (short-string->virtmach str)
;  "Split a string from vboxmanage list vms to a <virtmach>
;String must be in the form of, \"myvm\" {dce22e18-cf60-4a0a-9251-c87abd8c38f5}
;where the first string will be set to the virtmach->name and the second to
;the guid."
;  (let* ((split (string-split str #\ )))
;    (make-virtmach (string-delete #\" (car split))
;        (string-delete *curly* (cadr split)))))
;
;(define *curly* (char-set #\{ #\}))
;(define (get-all-vms)
;  (get-vms-cmd (string-append VBOX_CMD " list vms")))
;
;(define (get-running-vms)
;  (get-vms-cmd (string-append VBOX_CMD " list runningvms")))
;
;(define (get-vms-cmd cmd)
;  (let ((port (open-input-pipe cmd)))
;    (let next ((vms '()))
;      (let ((str (read-line port)))
;   (cond ((not (eof-object? str))
;          (next (append vms (list (short-string->virtmach str)))))
;         (else vms))))))
;
