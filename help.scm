#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder help)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:export (print
            pretty-obj
            newline-pred
            string-starts-with
            make-hyper-exception
            hyper-exception?
            make-hyper-storage-exception
            object->alist
            object->alist-parsable
            hyper-storage-exception?
            get-slots-accessor
            bytes->megabytes
            bytes->gigabytes
            megabytes->bytes
            gigabytes->bytes
            megabytes->bytes
            gigabytes->bytes
            remove-keys))

(define* (print #:key (sep " ") (end "\n") (port 'stdout) (flush #f) #:rest vals)
  ;(display vals) (display "\n")
  (define (b v)
    (let ((len (length v)))
      (cond ((< 1 len) (display (car v)) (display sep) (b (cdr v)))
        ((= 1 len) (display (car v)) (display end))
        (else (display end)))))
  (b (remove-keys vals '(#:sep #:end #:port #:flush))))

(define* (pretty-obj obj #:optional (fix-str? #t))
  (pretty-print (object->alist-parsable obj fix-str?)))

(define (remove-keys lst keys)
  (define (a l)
    (cond ((= 0 (length l)) l)
      (else
       (let ((iskey (member (car l) keys)))
         (cond ((not iskey)
            (cond ((= 1 (length l)) l)
              (else (cons (car l) (a (cdr l))))))
           (else
            (cond ((= 1 (length l)) '())
              (else (a (cddr l))))))))))
  (a lst))

(define (call-command-with-output-error-to-string cmd)
  (print cmd)
  (let* ((err-cons (pipe))
         (stdout-port '())
         (port (with-error-to-port (cdr err-cons)
                 (lambda ()
                   (set! stdout-port (open-input-pipe cmd))
                   stdout-port)))
         (_ (setvbuf (car err-cons) 'block
             (* 1024 1024 16)))
         (result (read-delimited "" port))
         (close (close-port (cdr err-cons)))
         (clpipe (close-pipe stdout-port))
         )
    (values
     (status:exit-val clpipe)
     close
     result
     (read-delimited "" (car err-cons)))))


(define* (string-starts-with starts in-str #:key (ci #f))
    (let ((comp (if ci string-ci=? string=?))
      (in-len (string-length in-str))
      (start-len (string-length starts)))
      (cond ((< in-len start-len) #f)
        (else (comp starts (substring in-str 0 start-len))))))


(define *NL* #\newline)
(define (newline-pred text pred)
  (map pred (string-split text *NL*)))

;; I ripped these from Guile source ice-9/exceptions.scm:108
(define-syntax define-hyper-exception-type-procedures
  (syntax-rules ()
    ((_ exception-type supertype constructor predicate
        (field accessor) ...)
     (begin
       (define constructor (record-constructor exception-type))
       (define predicate (exception-predicate exception-type))
       (define accessor (exception-accessor exception-type (record-accessor exception-type 'field)))
       ...))))

(define-syntax define-hyper-exception-type
  (syntax-rules ()
    ((_ exception-type supertype constructor predicate (field accessor) ...)
     (begin (define exception-type
              (make-record-type 'exception-type '((immutable field) ...)
                                #:parent supertype #:extensible? #t))
            (define-hyper-exception-type-procedures exception-type supertype constructor predicate (field accessor) ...)))))

(define-hyper-exception-type
  &hyper-exception
  &exception
  make-hyper-exception
  hyper-exception?
  (engine hyper-exception-engine) (cmd hyper-exception-cmd))

(define-hyper-exception-type
  &hyper-storage-exception
  &hyper-exception
  make-hyper-storage-exception
  hyper-storage-exception?
  (storage-obj hyper-storage-exception-storage-obj))


(define (get-slots-accessor klass)
  ;(if (not (is-a? <class> klass)) return
  (define (slots lst)
    (cond
     ((= 0 (length lst)) '())
     (else (cons (cons (slot-definition-init-keyword (car lst)) (slot-definition-name (car lst))) (slots (cdr lst))))))
  (slots (class-slots klass)))

(define (get-poopsslots-accessor klass)
  ;(if (not (is-a? <class> klass)) return
  (define (slots lst)
    (cond
     ((= 0 (length lst)) '())
     (else
      (append (list (slot-definition-init-keyword (car lst)) (slot-definition-name (car lst))) (slots (cdr lst))))))
  (slots (class-slots klass)))


(define (object->alist obj)
  (let* ((slots (class-slots (class-of obj))))
    (define (to-alist lst)
      (cond
       ((null? lst) '())
       (else
        (let ((slot (car lst)))
        (cons
         (cons (slot-definition-name slot) (apply (slot-definition-accessor slot) (list obj)))
         (to-alist (cdr lst)))))))
    (to-alist slots)))

(define* (object->alist-parsable obj #:optional (fix-str? #t))
  "This function will read a class instance and write out an alist that
    holds the values of each of the slots. When it hits a string it'll
    wrap it in quotes for reading/writing in/out from a file or string."
  (if (not (instance? obj)) obj
      (let* ((slots (class-slots (class-of obj))))
        (define (to-alist lst)
          (cond
           ((null? lst) '())
           (else
            (let* ((slot (car lst))
                   (val (apply (slot-definition-accessor slot) (list obj)))
                   (name (slot-definition-name slot)))
              (cond
                ; TODO Need to escape the values in val
               ((and (string? val))
                (if (not fix-str?) (cons (cons name val) (to-alist (cdr lst)))
                    (begin
                      (print "fixing string" val)
                      (cons (cons name (string-concatenate (list "\"" val "\""))) (to-alist (cdr lst))))))
               ((instance? val) (cons (cons name (object->alist-parsable val fix-str?)) (to-alist (cdr lst))))
               (else (cons (cons name val) (to-alist (cdr lst)))))))))
        (to-alist slots))))


(define (megabytes->bytes mb)
  (* mb 1000 1000))
(define (gigabytes->bytes gb)
  (* gb 1000 1000 1000))
(define (bytes->megabytes bytes)
  (/ bytes (* 1000 1000)))
(define (bytes->gigabytes bytes)
  (/ bytes (* 1000 1000 1000)))

;(define (str->human str)
;  )
;(define (alist->human alst)
;   )
;
;(define-syntax define-args
;  (syntax-rules ()
;    ((_ getter (field accessor) ...) (define getter
;     (lambda ()(list (field . accessor) ...))))))


;(define-args damn '(engine hyper-exception-engine) '(cmd hyper-exception-cmd) '(storage-obj hyper-storage-exception-storage-obj))
