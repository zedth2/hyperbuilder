#!/usr/bin/guile3.0

!#

(define-module (hyperbuilder help cmd)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-9)
  #:use-module (hyperbuilder help)
  #:export (<command>
            command->string-cmd
            build-command
            list->string-cmd
            make-command
            call-command-with-output-error-to-string
            command-stdout
            command-stderr
            command-exit-val
            command-term-sig
            command-stop-sig
            command-cmd
            command-args
            command?))

(define-record-type <command>
  (make-command cmd args)
  command?
  (cmd command-cmd)
  (args command-args)
  (stdout command-stdout set-command-stdout!)
  (stderr command-stderr set-command-stderr!)
  (exit-val command-exit-val set-command-exit-val!)
  (term-sig command-term-sig set-command-term-sig!)
  (stop-sig command-stop-sig set-command-stop-sig!))


(define* (build-command cmd #:rest args)
  (make-command cmd args))

(define (set-command cmd stdout stderr exit-val term-sig stop-sig)
  (set-command-stdout! cmd stdout)
  (set-command-stderr! cmd stderr)
  (set-command-exit-val! cmd exit-val)
  (set-command-term-sig! cmd term-sig)
  (set-command-stop-sig! cmd stop-sig)
  cmd)

(define (call-command-with-output-error-to-string cmd)
  (print "POOPS "(command->string-cmd cmd))
  (let* ((err-cons (pipe))
         (stdout-port '())
         (port (with-error-to-port (cdr err-cons)
                 (lambda ()
                   (set! stdout-port (open-input-pipe (command->string-cmd cmd)))
                   stdout-port)))
         (_ (setvbuf (car err-cons) 'block
                     (* 1024 1024 16)))
         (result (read-delimited "" port))
         (close (close-port (cdr err-cons)))
         (clpipe (close-pipe stdout-port))
         )
    (set-command cmd
                 result
                 (read-delimited "" (car err-cons))
                 (status:exit-val clpipe)
                 (status:term-sig clpipe)
                 (status:stop-sig clpipe))))
;    (values
;     (status:exit-val clpipe)
;     close
;     result
;     (read-delimited "" (car err-cons)))))

(define (command->string-cmd cmd)
  (print (command-cmd cmd))
  (print (command-args cmd))
  (list->string-cmd
   (cons (command-cmd cmd) (command-args cmd))))

(define (list->string-cmd lst)
  (define (b l)
    (cond ((eq? '() l) "")
      (else (string-concatenate (list (car l) " " (b (cdr l)))))))
  (b lst))
