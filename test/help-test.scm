#!/usr/bin/guile3.0

!#

(add-to-load-path "/home/zac/Documents/Code")

(define-module (hyperbuilder test help-test)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder help cmd)
  #:use-module (hyperbuilder help log)
  #:use-module (hyperbuilder builder qemu storage))

;(print (string-starts-with "Progress state:" "Progress state: VBOX_E_FILE_ERROR"))
;(print (string-starts-with "state:" "Progress state: VBOX_E_FILE_ERROR"))
;(print (string-starts-with "progress state:" "Progress state: VBOX_E_FILE_ERROR" #:ci #t))

;(print (create-img-cmd-str (make-storage "poops.img")))

;(print (call-command-with-output-error-to-string (build-command "./test/err.py")))

(debug #:sep "" "popos" 'bad 'for 'you)

(print #:sep "" 'sandwiches 'are 'tasty)
;(apply print (list #:sep "" 'sandwiches 'are 'tasty))
;

(define-class <numz> ()
  (the-number #:accessor the-num #:init-keyword #:the-number #:init-value 0))

(define-class <complex> ()
  (imagine #:accessor imagine #:init-keyword #:imagine #:init-value 'i))
