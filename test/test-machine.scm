#!/usr/bin/guile3.0

!#

(add-to-load-path "/home/zac/Documents/Code")
(define-module (hyperbuilder test test-machine)
  #:use-module (hyperbuilder help)
  #:use-module (hyperbuilder builder qemu machine)
  #:use-module (hyperbuilder builder qemu storage))

(define mach (make-machine 2 (* 2 1000 1000 1000) 'PIIX3 'x86_64 ""))
(define store (make-storage "poops.qcow2"))

(print (get-run-cmd mach store))
