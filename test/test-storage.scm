#!/usr/bin/guile3.0

!#

(add-to-load-path "/home/zac/Documents/Code")

(define-module (hyperbuilder test test-storage)
  #:use-module (hyperbuilder builder vbox storage)
  #:use-module (hyperbuilder builder storage)
  ;#:use-module (hyperbuilder builder qemu storage)
  #:use-module (hyperbuilder help))

;(define store (make-storage "poops.img" #:size-bytes 5000000))
;(print (create-img-cmd store))
;(create-img store)
;
;
;(define store (make-storage "dumb.img" #:vm-format "dumb" #:size-bytes 5000000))
;(print (create-img-cmd store))
;(create-img store)

;(define store (make-storage "crap.vdi" 5000000))
;(create-img store)

;(define store (make-storage "damn.vdi" 5000000))
;(create-img store)


(define hdd "UUID:           512dd25e-956c-4384-95f9-875054c10eed\nParent UUID:    2fea7c83-0e25-4b32-aa39-9c5738dfb874\nState:          created\nType:           normal (differencing)\nAuto-Reset:     off\nLocation:       /home/zac/VirtualBox VMs/clienttest/Snapshots/{512dd25e-956c-4384-95f9-875054c10eed}.vdi\nStorage format: VDI\nFormat variant: differencing default\nCapacity:       32768 MBytes\nSize on disk:   2329 MBytes\nEncryption:     disabled\nProperty:       AllocationBlockSize=1048576\nIn use by VMs:  clienttest (UUID: da7cff78-0159-4c74-8ea1-668e4a4f7e55)")
;(print (storage->human-str (make-storage-from-str hdd)))



;(print (get-slots-accessor <vbox-storage>))
;(print "\n\n")
;(print (get-slots-accessor <storage>))

;(print "\n\n")
(define s (object->alist-parsable (make-storage-from-str hdd) #f))
(print s)
(pretty-obj s #f)
